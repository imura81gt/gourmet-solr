var app = new Vue({
    el: "#app",
    data: {
        //url: 'http://localhost:8983/solr/ldgourmet/select?indent=on&wt=json',
        facets: [],
        response: [],
        station: "*",
        url: 'http://localhost:8080/solr/ldgourmet/select?facet.field={!ex=station}s_station_name&facet=on&facet.mincount=1&indent=on&q=*:*&rows=50&wt=json&fq={!tag=station}s_station_name:',
    },
    watch: {
    },
    created: function() {
        this.getFacets(this.station)
    },
    computed: {
        hasFacets: function() {
            return Object.keys(this.facets).length > 0 ? true : false;
        },
        hasResponse: function() {
            return Object.keys(this.response).length > 0 ? true : false;
        }
    },
    methods: {
        getFacets: function(station, e) {
            console.log("test" + station)
            var xhr = new XMLHttpRequest();
            var self = this;
            if (station != '*') {
              this.station = '"' + station + '"'
            }
            url = this.url + this.station
            console.log(url)
            xhr.open("GET", url);
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        //console.log(xhr.responseText)
                        json = JSON.parse(xhr.responseText)
                        //console.log(json)
                        f = json['facet_counts']['facet_fields']['s_station_name'];
                        var obj = {};
                        while (f.length) {
                            obj[f.shift()] = f.shift();
                        }
                        self.facets = obj
                        console.log(self.facets)
                        // r = json['response']['docs']
                        // while (r.length) {
                        //     obj[r.shift()] = r.shift();
                        // }
                        // self.response = obj
                        self.response = json['response']['docs']
                        console.log(self.response)
                    } else {
                        self.facets = [];
                        self.response = [];
                        console.error(xhr.statusText);
                    }
                }
                xhr.onerror = function (e) {
                        self.facets = [];
                        self.response = [];
                        console.error(xhr.statusText);
                };
            };
            xhr.send();
        }
    },
})
