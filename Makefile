all: get-data setup

setup: setup-mysql setup-solr

clean: clean-solr clean-ldgourmet

get-data:
	cd mysql ;\
	python get_data.py ;

setup-mysql:
	cd mysql ;\
	embulk run areas/config.yml ;\
	embulk run categories/config.yml ;\
	embulk run prefs/config.yml ;\
	embulk run rating_votes/config.yml ;\
	embulk run restaurants/config.yml ;\
	embulk run stations/config.yml ;\
	embulk run ratings/config.yml ;

setup-solr:
	cd solr ;\
	python full-import.py

clean-solr:
	rm -rf srv/docker/solr/

clean-ldgourmet:
	rm -rf mysql/data/

