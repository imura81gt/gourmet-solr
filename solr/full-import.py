from urllib.request import urlopen
from urllib.parse import urlencode

SCHEMA_PATH = 'ldgourmet/'
DIH_PATH = 'dataimport?'
URL = 'http://localhost:18983/solr/' + SCHEMA_PATH + DIH_PATH

options = {
    'command': 'full-import',
    'clean': 'true',
    'commit': 'true',
    'entity': 'ldgourmet',
}


def full_import():
    url = URL + urlencode(options)
    ret = urlopen(url)
    return ret


def main():
    print(full_import())


if __name__ == "__main__":
    main()
