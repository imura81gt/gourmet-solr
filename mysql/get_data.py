from urllib.request import urlretrieve
from urllib.parse import urlparse
import os
import tarfile

URL = "https://github.com/livedoor/datasets/raw/master/ldgourmet.tar.gz"


def main():
    file_name = urlparse(URL).path.split('/')[-1]
    download_path = "data"
    if not os.path.exists(download_path):
        os.mkdir(download_path)
    file_path = os.path.join(download_path, file_name)
    if not os.path.exists(file_path):
        print("downloading..")
        urlretrieve(URL, filename=file_path)
    with tarfile.open(file_path, 'r') as f:
        f.extractall(download_path)


if __name__ == "__main__":
    main()
