Install Embulk
---

https://github.com/embulk/embulk


Install Embulk Plugins
---

```
$ git clone https://gitlab.com/imura81gt/gourmet-solr.git
$ cd gourmet-solr/
$ embulk bundle install
```

Start Services
---

Starting MySQL and Solr Containers.
MySQL と Solr を起動する

```
$ docker-compose up
```

Update Data
---


```
$ make all
```

livedoor/datasets: https://github.com/livedoor/datasets

1. embulk import Data from csv to MySQL.
1. Solr DataImportHandler get data from MySQL.

- embulk で csv を MySQL に INSERT する
- Solr DataImportHandlerでSolrがMySQLからデータを取ってきてIndexする

Access Solr Management Console
---

### Excecute Query

http://localhost:18983/solr/#/ldgourmet/query

Click `Excecute Query`


### Excecute Query

http://localhost:18983/solr/#/ldgourmet/schema?field=s_station_name

Click `Loat Term Info`


Let's search!
===================================


spacial search
-------------------

e.g. Tokyo Station, 1km
http://localhost:18983/solr/ldgourmet/select?d=1&indent=on&pt=35.681167,139.767052&q={!geofilt%20sfield=loc_p}&sfield=loc_p&wt=json

